connection: "lookerdb"

# include all the views
include: "/views/**/*.view"

datagroup: citadel_daily_call_records_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: citadel_daily_call_records_default_datagroup

explore: citadelllcdailycallrecords {}

# explore: call_records {}
