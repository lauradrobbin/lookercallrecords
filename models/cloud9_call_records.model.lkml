connection: "lookerdb"

# include all the views
include: "/views/**/*.view"

datagroup: cloud9_daily_call_records_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: cloud9_daily_call_records_default_datagroup

explore: cloud9_daily_call_records {}
