view: cloud9_daily_call_records {
  sql_table_name: lookerdb.Cloud9DailyCallRecords ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  #dimension: date {
  #  type: string
  #  sql: ${TABLE}.date ;;
  #}

  dimension: far_end_firm_name {
    type: string
    sql: ${TABLE}.far_end_firm_name ;;
  }

  dimension: far_end_group_id {
    type: number
    sql: ${TABLE}.far_end_group_id ;;
  }

  dimension: far_end_group_name {
    type: string
    sql: ${TABLE}.far_end_group_name ;;
  }

  dimension: near_end_group_id {
    type: number
    sql: ${TABLE}.near_end_group_id ;;
  }

  dimension: near_end_group_name {
    type: string
    sql: ${TABLE}.near_end_group_name ;;
  }

  dimension: near_end_username {
    type: string
    sql: ${TABLE}.near_end_username ;;
  }

  dimension: start_time_bucket {
    type: string
    sql: ${TABLE}.start_time_bucket ;;
  }

  dimension_group: start_time {
    type: time
    timeframes: [hour_of_day, minute15, time_of_day, date, day_of_week, week, month, year]
    datatype: epoch
    sql: ${TABLE}.start_time_bucket_unix ;;
  }

  measure: callcount_per_bucket {
    type: number
    sql: ${TABLE}.callcount ;;
  }

  measure: call_count {
    type: sum
    sql: ${TABLE}.callcount ;;
  }

  measure: duration_mean_per_bucket {
    type: number
    sql: ${TABLE}.duration_mean ;;
  }

  measure: duration_sum_per_bucket {
    type: number
    sql: ${TABLE}.duration_sum ;;
  }

  measure: duration_sum {
    type: sum
    sql: ${TABLE}.duration_sum ;;
  }

  measure: duration_mean {
    type: number
    sql: round(${duration_sum}/${call_count},1) ;;
  }

  measure: qos_mean_per_bucket {
    type: number
    sql: ${TABLE}.QOS_mean ;;
  }

  measure: qos_total_per_bucket {
    type: sum
    sql: (${TABLE}.QOS_mean * ${TABLE}.callcount) ;;
  }

  measure: qos_mean {
    type: number
    sql: ${qos_total_per_bucket}/${call_count} ;;
  }

  measure: count {
    type: count
    drill_fields: [id, near_end_username, near_end_group_name, far_end_firm_name, far_end_group_name]
  }
}
