view: citadelsecuritiesdailycallrecords {
  sql_table_name: lookerdb.citadelsecuritiesdailycallrecords ;;

  dimension: callcount {
    type: number
    sql: ${TABLE}.callcount ;;
  }

  dimension: date {
    type: string
    sql: ${TABLE}.date ;;
  }

  dimension: duration_mean {
    type: number
    sql: ${TABLE}.duration_mean ;;
  }

  dimension: duration_sum {
    type: number
    sql: ${TABLE}.duration_sum ;;
  }

  dimension: far_end_firm_name {
    type: string
    sql: ${TABLE}.far_end_firm_name ;;
  }

  dimension: far_end_group_id {
    type: number
    sql: ${TABLE}.far_end_group_id ;;
  }

  dimension: far_end_group_name {
    type: string
    sql: ${TABLE}.far_end_group_name ;;
  }

  dimension: near_end_group_id {
    type: number
    sql: ${TABLE}.near_end_group_id ;;
  }

  dimension: near_end_group_name {
    type: string
    sql: ${TABLE}.near_end_group_name ;;
  }

  dimension: near_end_username {
    type: string
    sql: ${TABLE}.near_end_username ;;
  }

  dimension: qos_mean {
    type: number
    sql: ${TABLE}.QOS_mean ;;
  }

  dimension: record {
    type: number
    sql: ${TABLE}.record ;;
  }

  dimension: start_time_bucket {
    type: string
    sql: ${TABLE}.start_time_bucket ;;
  }

  dimension: start_time_bucket_unix {
    type: number
    sql: ${TABLE}.start_time_bucket_unix ;;
  }

  measure: count {
    type: count
    drill_fields: [near_end_username, near_end_group_name, far_end_firm_name, far_end_group_name]
  }
}
