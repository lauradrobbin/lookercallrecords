view: citadel_union {
  derived_table: {
    sql: select * from lookerdb.citadelsecuritiesdailycallrecords where start_time_bucket_unix > 1633482001
      ;;
  }

  dimension: id {
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: near_end_username {
    type: string
    sql: ${TABLE}.near_end_username ;;
  }

  dimension: date {
    type: string
    sql: ${TABLE}.date ;;
  }

  dimension: start_time_bucket {
    type: string
    sql: ${TABLE}.start_time_bucket ;;
  }

  dimension: start_time_bucket_unix {
    type: number
    sql: ${TABLE}.start_time_bucket_unix ;;
  }

  dimension_group: start_time {
    type: time
    timeframes: [hour_of_day, minute15, time_of_day, date, day_of_week, week, month, year]
    datatype: epoch
    sql: ${TABLE}.start_time_bucket_unix ;;
  }

  dimension: near_end_group_id {
    type: number
    sql: ${TABLE}.near_end_group_id ;;
  }

  dimension: near_end_group_name {
    type: string
    sql: ${TABLE}.near_end_group_name ;;
  }

  dimension: far_end_firm_name {
    type: string
    sql: ${TABLE}.far_end_firm_name ;;
  }

  dimension: far_end_group_id {
    type: number
    sql: ${TABLE}.far_end_group_id ;;
  }

  dimension: far_end_group_name {
    type: string
    sql: ${TABLE}.far_end_group_name ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: duration_sum {
    type: number
    sql: ${TABLE}.duration_sum ;;
  }

  measure: duration_mean {
    type: number
    sql: ${TABLE}.duration_mean ;;
  }

  measure: callcount_per_bucket {
    type: number
    sql: ${TABLE}.callcount ;;
  }

  measure: call_count {
    type: sum
    sql: ${TABLE}.callcount ;;
  }

  measure: duration_mean_per_bucket {
    type: number
    sql: ${TABLE}.duration_mean ;;
  }

  measure: duration_sum_per_bucket {
    type: number
    sql: ${TABLE}.duration_sum ;;
  }

  measure: qos_mean {
    type: number
    sql: ${TABLE}.QOS_mean ;;
  }

  set: detail {
    fields: [
      id,
      near_end_username,
      date,
      start_time_bucket,
      start_time_bucket_unix,
      near_end_group_id,
      near_end_group_name,
      far_end_firm_name,
      far_end_group_id,
      far_end_group_name,
      duration_sum,
      duration_mean,
      call_count,
      qos_mean
    ]
  }
}
